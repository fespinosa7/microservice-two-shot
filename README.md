# Wardrobify

Team:

* Person 1 - John Blanton - Hats
* Person 2 - Franz Espinosa - Shoes

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

## Hats microservice

The hats model contains fabric, style name, color, a picture url, and a location. The locations come from the shared (with the shoes microservice) wardrobe API's. The hats microservice uses polling to ensure the location data is puled when needed from the wardrobe API's. 

