import React, { useEffect, useState } from "react";


export default function ShoesList() {

  const [shoes, setShoes] = useState([]);
  const fetchData = async () => {
    const url = "http://localhost:8080/api/shoes/"

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setShoes(data.shoes);
    }
  };

  function deleteShoe(id) {
    const url = `http://localhost:8080/api/shoes/${id}/`;

    const options = {
      method: "DELETE",
    };
    fetch(url, options)
      .then((result) => {
        return result.json();
      })
      .then((resp) => {
        console.warn(resp);
        window.location.reload();
      });
  }


  useEffect(() => {
    fetchData();
  }, []);
  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Manufacturer</th>
          <th>Model Name</th>
          <th>Color</th>
          <th>Photo</th>
        </tr>
      </thead>
      <tbody>
        {shoes.map((shoe) => {
          return (
            <tr key={shoe.id}>
              <td>{shoe.manufacturer}</td>
              <td>{shoe.model_name}</td>
              <td>{shoe.color}</td>
              <td>{shoe.url}</td>
              <td>
                <button className="btn btn-danger" onClick={() => deleteShoe(shoe.id)}>Delete</button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}
