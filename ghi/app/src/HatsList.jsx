import { useState, useEffect } from "react";
import { Link } from 'react-router-dom';

function HatList(props) {
    const [hats, setHats] = useState([]);

    async function fetchHats() {
    const response = await fetch('http://localhost:8090/hats/');

    if (response.ok) {
        const data = await response.json();
        setHats(data.hats);
        }
    }

    useEffect(() => {
        fetchHats();
    }, [])
    const deleteHat = async (id) => {
        const response = await fetch(`http://localhost:8090/hats/${id}`, {
            method: 'DELETE'
        });
        if (response.ok) {
            fetchHats();
        }
    }
    return (
        <>
        <div className="d-flex justify-content-center">
            <Link to="/hats/add" className="btn btn-success btn-lg px-4 my-4">Add a Hat</Link>
        </div>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Picture</th>
                    <th>Style Name</th>
                    <th>Fabric</th>
                    <th>Color</th>
                    <th>Location</th>
                </tr>
            </thead>
            <tbody>
                {hats.map(hat => {
                    return (
                        <tr key={hat.id}>
                            <td ><img src={hat.picture_url} className="img-thumbnail" alt='' style={{ maxHeight: '100px' }}/></td>
                            <td>{ hat.style_name }</td>
                            <td>{ hat.fabric }</td>
                            <td>{ hat.color }</td>
                            <td>{ hat.location.name }</td>
                            <td>
                                <button onClick={() => deleteHat(hat.id)} className="btn btn-outline-danger btn-sm">Delete Hat</button>
                            </td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
        </>
    )
};

export default HatList;
