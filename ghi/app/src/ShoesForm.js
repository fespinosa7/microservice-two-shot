import React, { useEffect, useState } from 'react';
import { useNavigate } from "react-router-dom";


const ShoesForm = () => {
  const [bins, setBins] = useState([]);

  const [name, setName] = useState('');
  const [manufacturer, setManufacturer] = useState('');
  const [modelName, setModelName] = useState('');
  const [color, setColor] = useState('');
  const [pictureURL, setPictureURL] = useState('');
  const [bin, setBin] = useState('');

  const navigate = useNavigate();

  const fetchData = async () => {
    const url = "http://localhost:8100/api/bins/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setBins(data.bins);
    }
  }

  const handleNameChange = (event) => {
      const value = event.target.value;
      setName(value);
  }

  const handleManufacturerChange = (event) => {
      const value = event.target.value;
      setManufacturer(value);
  }

  const handleModelNameChange = (event) => {
      const value = event.target.value;
      setModelName(value);
  }

  const handleColorChange = (event) => {
      const value = event.target.value;
      setColor(value);
  }

  const handlePictureURLChange = (event) => {
      const value = event.target.value;
      setPictureURL(value);
  }

  const handleBinChange = (event) => {
      const value = event.target.value;
      setBin(value);
  }

  const handleSubmit = async (event) => {
      event.preventDefault();

      const data = {};

      data.name = name;
      data.manufacturer = manufacturer;
      data.model_name = modelName;
      data.color = color;
      data.picture_url = pictureURL;
      data.bin = bin;

      const shoesUrl = `http://localhost:8080/api/shoes/`;
      const fetchConfig = {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
      };
      const response = await fetch(shoesUrl, fetchConfig);
      console.log(response)
      if (response.ok) {

          setName('');
          setManufacturer('');
          setModelName('');
          setColor('');
          setPictureURL('');
          setBin('');
      }
      navigate('/shoes');
    }

    useEffect(() => {
      fetchData();
    }, []);

  return (
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a new pair of kicks!</h1>
            <form onSubmit={handleSubmit} id="create-shoe-form">

              <div className="form-floating mb-3">
                <input onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" value={name}/>
                <label htmlFor="name">Name</label>
              </div>

              <div className="form-floating mb-3">
                <input onChange={handleManufacturerChange} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" value={manufacturer}/>
                <label htmlFor="manufacturer">Manufacturer</label>
              </div>

              <div className="form-floating mb-3">
                <input onChange={handleModelNameChange} placeholder="Model Name" required type="text" name="model_name" id="model_name" className="form-control" value={modelName}/>
                <label htmlFor="model_name">Model Name</label>
              </div>

              <div className="form-floating mb-3">
                <input onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" value={color}/>
                <label htmlFor="color">Color</label>
              </div>

              <div className="form-floating mb-3">
                <input onChange={handlePictureURLChange} placeholder="Picture URL" required type="url" name="picture_url" id="picture_url" className="form-control" value={pictureURL}/>
                <label htmlFor="picture_url">Picture URL</label>
              </div>

              <div className="mb-3">
                <select onChange={handleBinChange} required name="bin" id="bin" className="form-select" value={bin}>
                <option value=''>Choose a bin</option>
                {bins.map(bin => {
                    return (
                        <option key={bin.href} value={bin.href}>
                            {bin.closet_name}
                        </option>
                    );
                })}
                </select>
              </div>

              <button className="btn btn-primary">Create 'em!</button>
            </form>
          </div>
        </div>
      </div>
  )
}

export default ShoesForm
