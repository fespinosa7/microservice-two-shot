import React, {useEffect, useState} from 'react';


function HatForm(){
  const [locations, setLocations] = useState([])
  const [location, setLocation] = useState("");
  const [fabric, setFabric] = useState("");
  const [style, setStyle] = useState("");
  const [color, setColor] = useState("");
  const [url, setUrl] = useState("");


  const getLocationData = async () => {
    try {
      const response = await fetch("http://localhost:8100/api/locations/");
      const data = await response.json();
      setLocations(data.locations);
    } catch (err) {
      console.error(err);
    }
  };

  const HandleLocationChange = (event) => {
    const value = event.target.value;
    setLocation(value);
  }

  const HandleFabricChange = (event) => {
    const value = event.target.value;
    setFabric(value);
  }

  const HandleStyleChange = (event) => {
    const value = event.target.value;
    setStyle(value);
  }

  const HandleColorChange = (event) => {
    const value = event.target.value;
    setColor(value);
  }

  const HandleUrlChange = (event) => {
    const value = event.target.value;
    setUrl(value);
  }

  const HandleSubmit= async (event) => {
    event.preventDefault()

    const data = {}
    data.fabric = fabric
    data.style_name = style
    data.color = color
    data.picture_url = url
    data.location = location

    const hatUrl = "http://localhost:8090/hats/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(hatUrl, fetchConfig);
    if (response.ok) {
      setFabric("");
      setStyle("");
      setColor("");
      setUrl("");
      setLocation("");
    }
  };

  useEffect(() => {
    getLocationData();
  }, []);
  return (
    <div className="row">
    <div className="offset-3 col-6">
      <div className="shadow p-4 mt-4">
        <h1 className="text-center">Add a hat</h1>
        <form  onSubmit={HandleSubmit} id="create-presentation-form">
          <div className="form-floating mb-3">
            <input
              placeholder="Fabric"
              onChange={HandleFabricChange}
              value={fabric}
              required
              type="text"
              name="fabric"
              id="fabric"
              className="form-control"
            />
            <label htmlFor="fabric">Fabric</label>
          </div>
          <div className="form-floating mb-3">
            <input
              placeholder="Style"
              onChange={HandleStyleChange}
              value={style}
              required
              type="text"
              name="style"
              id="style"
              className="form-control"
            />
            <label htmlFor="style">Style</label>
          </div>
          <div className="form-floating mb-3">
            <input
              placeholder="Color"
              value={color}
              onChange={HandleColorChange}
              required
              type="text"
              className="form-control"
              id="color"
              name="color"
            />
            <label htmlFor="picture_url">Color</label>
            </div>
          <div className="form-floating mb-3">
            <input
            placeholder="Picture URL"
            onChange={HandleUrlChange}
            required
            value={url}
            type="url"
            name="picture_url"
            id="picture_url"
            className="form-control"
            />
            <label htmlFor="picture_url">Picture URL</label>
          </div>
          <div className="mb-3">
            <select
            onChange={HandleLocationChange}
              value={location}
              required
              name="conference"
              id="conference"
              className="form-select"
            >
              {locations.map((location) => {
                  return (
                    <option key={location.href} value={location.href}>
                      {location.closet_name} Section {location.section_number} Shelf {location.shelf_number}
                    </option>
                  );
                })}
              <option value="">Choose a location</option>
            </select>
          </div>
          <button className="btn btn-primary">Add</button>
        </form>
      </div>
    </div>
  </div>
  )
}

export default HatForm
