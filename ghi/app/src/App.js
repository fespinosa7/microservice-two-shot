import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesForm from "./ShoesForm";
import ShoesList from "./ShoesList";
import HatForm from './HatForm'
import HatsList from './HatsList'

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes" element={<ShoesList shoes={props.shoes}/>}/>
          <Route path='shoes/new' element={<ShoesForm/>}/>
          <Route path="hats">
            <Route path="" element = {<HatsList />} />
            <Route path ="add" element= {<HatForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
