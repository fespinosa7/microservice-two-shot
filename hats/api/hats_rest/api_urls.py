from django.urls import path
from .views import api_list_hats, api_show_hat

urlpatterns = [
    path("", api_list_hats, name='api_list_hats'),
    path("<int:id>", api_show_hat, name="api_show_hat")
]
