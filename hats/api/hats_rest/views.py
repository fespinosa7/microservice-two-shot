from .models import LocationVO, Hats
from common.json import ModelEncoder
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json


class LocationEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "name",
        "href"
    ]


class HatsEncoder(ModelEncoder):
    model = Hats
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
        "id"
    ]
    encoders = {
        "location": LocationEncoder()
    }


@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hats.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatsEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            location_url = content["location"]
            location = LocationVO.objects.get(href=location_url)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        hat = Hats.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatsEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET"])
def api_show_hat(request, id):
    if request.method == "GET":
        try:
            hat = Hats.objects.get(id=id)
            return JsonResponse({"Hat": hat}, HatsEncoder, safe=False)
        except hat.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    elif request.method == "DELETE":
        count, _ = Hats.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
