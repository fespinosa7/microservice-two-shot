from common.json import ModelEncoder
from .models import Shoe, BinVO


class BinEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "href",
    ]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "name",
        "color",
        "bin",
        "id",
    ]
    encoders = {
        "bin": BinEncoder()
    }

    # def get_extra_data(self, o):
    #     return {"bin": o.bin.closet_name}


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "name",
        "color",
        "href",
        "bin",
    ]
    encoders = {
        "bin": BinEncoder(),
    }
