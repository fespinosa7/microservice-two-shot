from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import BinVO, Shoe
from .encoders import ShoeDetailEncoder, ShoeListEncoder
import json

# Create your views here.


@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)

        try:
            bin_location = content["bin"]
            bin = BinVO.objects.get(href=bin_location)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin"},
                status=400,
            )

        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeListEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_shoe(request, pk):
    if request.method == "GET":
        shoe = Shoe.objects.get(id=pk)
        return JsonResponse(shoe, encoder=ShoeDetailEncoder, safe=False)

    elif request.method == "DELETE":
        count, _ = Shoe.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})

    else:
        content = json.loads(request.body)

        try:
            bin_href = content['bin']
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except:
            return JsonResponse({"message": "Valid bin"})

        Shoe.objects.filter(id=pk).update(**content)

        new_shoe = Shoe.objects.get(id=pk)
        return JsonResponse(new_shoe, encoder=ShoeDetailEncoder, safe=False)
